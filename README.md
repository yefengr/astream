# AStream

## 介绍

Java搭建的流媒体服务器。

spring boot项目，纯Java实现，不使用任何三方工具。 基于Java CV实现转码操作。

1. 实现RTSP/RTMP转Http-Flv、Hls
2. 支持转复用与转码两种形式
3. 记录客户端连接数量，没有客户端连接时，自动关闭服务

## 接口
```

### 开始流服务，调用后，发挥播放地址
POST http://localhost:15006/stream/start

### 定制流服务
POST http://localhost:15006/stream/stop

### 检测某个摄像头是否在线
POST http://localhost:15006/stream/isOnline

### 查询当前开始的所有服务
GET http://localhost:15006/stream/listLives

### Http-Flv播放地址
GET http://localhost:15006/stream/getFly/{{playAddress}}

### Hls播放地址
GET http://localhost:15006/stream/getHls/{{playAddress}}/{{playName}}
```


实现原理：https://juejin.cn/post/6971439103967494152/

## 在线rtsp流播放地址

https://www.cnblogs.com/jing99/p/14722697.html

## 更新日志

### 2021-11-18

1. 优化摄像头在线状态检测，通过检测ip和端口是否可连接，来确定摄像头是否在线。

### 2021-06-16

1. 参考大佬代码，将hls生成片段存在缓存中。
2. 同时使用https协议与http协议。http协议用于本地保存hls切片；https协议用于对外访问。

## 参考

本项目参考大佬的项目实现：https://gitee.com/52jian/EasyMedia