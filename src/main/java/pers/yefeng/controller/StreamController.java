package pers.yefeng.controller;

import pers.yefeng.base.View;
import pers.yefeng.service.StreamService;
import pers.yefeng.vo.StreamInfoV0;
import pers.yefeng.vo.StreamParamV0;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *
 * @date 2021/05/24 10:47:16
 */
@RestController
@RequestMapping("/stream")
public class StreamController {


    @Autowired
    private StreamService streamService;

    /**
     * 开始流服务
     *
     * @param streamParamV0 参数信息
     * @return 返回直播流地址
     */
    @PostMapping("/start")
    public View<String> start(
            @RequestBody StreamParamV0 streamParamV0) {
        return streamService.start(streamParamV0);
    }

    /**
     * 停止流服务
     *
     * @param streamParamV0 参数信息
     * @return view
     */
    @PostMapping("/stop")
    public View<String> stop(
            @RequestBody StreamParamV0 streamParamV0) {
        return streamService.stop(streamParamV0);
    }


    /**
     * 检测摄像头是否在线。因为是通过获取rtsp流检测。所以入参直接为rtsp流
     *
     * @param sourceAddress 视频原始地址
     * @return view。成功为true
     */
    @PostMapping("/isOnline")
    public View<Boolean> isOnline(
            @RequestParam String sourceAddress) {
        return streamService.isOnline(sourceAddress);
    }


    /**
     * 获取所有在线的流服务
     *
     * @return list
     */
    @GetMapping("/listLives")
    public View<List<StreamInfoV0>> listLives() {
        return streamService.listLives();
    }

    /**
     * 获取flv格式视频流
     *
     * @param playAddress 播放地址
     */
    @GetMapping("/getFly/{playAddress}")
    public void getFly(
            @PathVariable("playAddress") String playAddress) {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletResponse response = servletRequestAttributes.getResponse();
        HttpServletRequest request = servletRequestAttributes.getRequest();


        streamService.getFly(playAddress, request, response);
    }

    /**
     * 获取Hls格式视频流
     *
     * @param playAddress 播放地址
     */
    @GetMapping("/getHls/{playAddress}/{playName}")
    public void getHls(
            @PathVariable("playAddress") String playAddress,
            @PathVariable("playName") String playName

    ) {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletResponse response = servletRequestAttributes.getResponse();
        HttpServletRequest request = servletRequestAttributes.getRequest();
        streamService.getHls(playAddress, playName, request, response);
    }

    /**
     * 记录hls片段
     *
     * @param request
     * @param playAddress
     * @param playName
     * @return
     */
    @RequestMapping("/recordHls/{playAddress}/{playName}")
    public void recordHls(
            HttpServletRequest request,
            @PathVariable("playAddress") String playAddress,
            @PathVariable("playName") String playName) {
        streamService.recordHls(request, playAddress, playName);
    }



}
