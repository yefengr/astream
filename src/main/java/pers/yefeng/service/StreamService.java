package pers.yefeng.service;

import pers.yefeng.base.View;
import pers.yefeng.vo.StreamInfoV0;
import pers.yefeng.vo.StreamParamV0;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 流媒体服务
 *
 *
 * @date 2021/05/24 11:27:16
 */
public interface StreamService {

    /**
     * 开始流服务
     *
     * @param streamParamV0 参数信息
     * @return 返回直播流地址
     */
    View<String> start(StreamParamV0 streamParamV0);

    /**
     * 停止流服务
     *
     * @param streamParamV0
     * @return view
     */
    View<String> stop(StreamParamV0 streamParamV0);

    /**
     * 获取所有在线的流服务
     *
     * @return list
     */
    View<List<StreamInfoV0>> listLives();

    /**
     * 获取flv格式视频流
     *
     * @param playAddress 播放地址
     * @param request
     */
    void getFly(String playAddress, HttpServletRequest request, HttpServletResponse response);

    /**
     * 获取flv格式视频流
     *
     * @param playAddress 播放地址
     * @param playName    文件名称
     * @param request     request
     * @param response    response
     */
    void getHls(String playAddress, String playName, HttpServletRequest request, HttpServletResponse response);

    /**
     * 检测摄像头是否在线。因为是通过获取rtsp流检测。所以入参直接为rtsp流
     *
     * @param sourceAddress 视频原始地址
     * @return view
     */
    View<Boolean> isOnline(String sourceAddress);

    /**
     * 记录hls片段
     *
     * @param request
     * @param playAddress
     * @param playName
     */
    void recordHls(HttpServletRequest request, String playAddress, String playName);



}
