package pers.yefeng.utils;

import pers.yefeng.constant.StreamConstant;
import pers.yefeng.thread.MediaThread;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 *
 * @date 2021/05/24 16:12:13
 */
public class StreamUtil {

    /**
     * 获取视频播放地址
     *
     * @param playAddress 播放地址
     * @return 返回线程对象
     */
    public static MediaThread getPlayAddress(String playAddress) {
        if (StringUtils.isBlank(playAddress)) {
            return null;
        }
        for (Map.Entry<String, MediaThread> entry : StreamConstant.MEDIA_INFO_MAP.entrySet()) {
            String sourceAddressInMap = entry.getKey();
            MediaThread value = entry.getValue();
            String playAddressThis = value.getPlayAddress();
            if (playAddress.equals(playAddressThis)) {
                return value;
            }
        }
        return null;
    }


    /**
     * 生成可用的播放地址
     *
     * @return str
     */
    public static String generatePlaybackAddress() {
        String random = RandomStringUtils.randomAlphanumeric(9);
        boolean available = true;
        for (Map.Entry<String, MediaThread> entry : StreamConstant.MEDIA_INFO_MAP.entrySet()) {
            String playAddress = entry.getValue().getPlayAddress();
            if (random.equals(playAddress)) {
                available = false;
                break;
            }
        }
        if (available) {
            return random;
        }
        return generatePlaybackAddress();
    }


    /**
     * 获取用户真实IP地址，不使用request.getRemoteAddr();的原因是有可能用户使用了代理软件方式避免真实IP地址,
     * 参考文章： http://developer.51cto.com/art/201111/305181.htm
     * <p>
     * 可是，如果通过了多级反向代理的话，X-Forwarded-For的值并不止一个，而是一串IP值，究竟哪个才是真正的用户端的真实IP呢？
     * 答案是取X-Forwarded-For中第一个非unknown的有效IP字符串。
     * <p>
     * 如：X-Forwarded-For：192.168.1.110, 192.168.1.120, 192.168.1.130,
     * 192.168.1.100
     * <p>
     * 用户真实IP为： 192.168.1.110
     *
     * @param request
     * @return
     */
    public static String getIpAddress(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }







}
