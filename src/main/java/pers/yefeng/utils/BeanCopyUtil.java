package pers.yefeng.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.util.Assert;

import java.lang.reflect.Field;
import java.util.*;


@Slf4j
public class BeanCopyUtil {


    public static <T, V> List<V> copyListDTO(Collection<T> sourceList, Class<V> target) {
        Assert.notNull(sourceList, "SourceList must not be null");
        Assert.notNull(target, "Target must not be null");
        return JSON.parseArray(JSON.toJSONString(sourceList), target);
    }

}
