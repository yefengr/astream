package pers.yefeng.thread;

import pers.yefeng.constant.StreamConstant;
import pers.yefeng.vo.StreamParamV0;

/**
 *
 * @date 2021/05/28 14:55:03
 */
public class MediaThreadBuilder {
    public static MediaThread create(StreamParamV0 streamParamV0) {
        if (StreamConstant.VIDEO_TYPE_FLV.equals(streamParamV0.getVideoType())) {
            return new FlvMediaThread(streamParamV0);
        }
        return new HlsMediaThread(streamParamV0);
    }
}
