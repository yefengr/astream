package pers.yefeng.constant;

import pers.yefeng.thread.MediaThread;

import java.util.concurrent.ConcurrentHashMap;

/**
 * 静态常量
 *
 *
 * @date 2021/05/24 10:36:08
 */
public class StreamConstant {


    /**
     * 视频类型 ：flv、hls
     */
    public static final String VIDEO_TYPE_FLV = "flv";
    public static final String VIDEO_TYPE_HLS = "hls";

    /**
     * 临时文件名称
     */
    public static final String VIDEO_FILE_NAME_HLS = "play.m3u8";
    /**
     * 临时文件名称
     */
    public static final String VIDEO_FILE_NAME = "play";

    /**
     * 播放地址
     */
    public static final String URL_PLAY_FLV = "/stream/getFly/";
    public static final String URL_PLAY_HLS = "/stream/getHls/";


    /**
     * 视频流转换类型 自动（auto），转码（transcoding），转复用（reuse）
     */
    public static final String CONVERSION_TYPE_AUTO = "auto";
    public static final String CONVERSION_TYPE_TRANSCODING = "transcoding";
    public static final String CONVERSION_TYPE_REUSE = "reuse";

    /**
     * 转流服务开启进度
     */
    public static final int SERVICE_START_PROGRESS_PROCESSING = 1;
    public static final int SERVICE_START_PROGRESS_SUCCESS = 2;
    public static final int SERVICE_START_PROGRESS_FAILURE = 3;


    /**
     * 缓存流转换信息 key：视频源地址，value：视频信息
     */
    public static ConcurrentHashMap<String, MediaThread> MEDIA_INFO_MAP = new ConcurrentHashMap<>();

    /**
     * 记录开启转流服务时的执行信息
     */
//    public static ConcurrentHashMap<String, View<String>> START_VIEW_MAP = new ConcurrentHashMap<>();


    /**
     * hls临时文件生成目录
     */
    public static String HLS_DIR = null;
    /**
     * 端口号
     */
    public static Integer PORT = null;
    /**
     * 是否开启
     */
    public static Boolean OPEN_SSL = false;

    /**
     * http协议端口
     */
    public static  Integer HTTP_PORT = null;


}
