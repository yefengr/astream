package pers.yefeng.vo;


import lombok.Data;
import lombok.Getter;

import javax.servlet.ServletOutputStream;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 流媒体详情信息
 *
 *
 * @date 2021/05/24 10:20:37
 */
@Data
public class StreamInfoV0 {

    private String playAddress;

    /**
     * 摄像头地址
     */
    String cameraAddress = "";

    /**
     * 视频类型：flv、hls
     * 不传默认flv
     */
    String videoType;


    /**
     * 流转换类型 自动（auto），转码（transcoding），转复用（reuse）
     */
    String conversionType;

    /**
     * 客户端在线数量
     */

    ConcurrentHashMap<String, Long> httpMap = new ConcurrentHashMap<>();


}
