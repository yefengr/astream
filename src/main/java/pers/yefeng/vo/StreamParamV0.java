package pers.yefeng.vo;


import lombok.Data;
import lombok.Getter;


/**
 * 流媒体参数信息
 *
 *
 * @date 2021/05/24 10:20:37
 */
@Data
public class StreamParamV0 {

    /**
     * 视频地址
     */
    String sourceAddress;


    /**
     * 视频类型：flv、hls
     * 不传默认flv
     */
    String videoType;


    /**
     * 流转换类型 自动（auto），转码（transcoding），转复用（reuse）
     */
    String conversionType;


}
