package pers.yefeng.config;

import pers.yefeng.constant.StreamConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 启动初始化
 *
 *
 * @date 2021/05/26 10:08:43
 */
@Slf4j
@Component
class ApplicationRunnerInit implements ApplicationRunner {


    @Value("${server.port}")
    private Integer port;
    @Value("${my-config.http.port}")
    private Integer httpPort;


    @Resource(name = "executor")
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        StreamConstant.PORT = port;
        StreamConstant.HTTP_PORT = httpPort;
    }

}
