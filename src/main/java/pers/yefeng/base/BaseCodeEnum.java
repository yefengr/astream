package pers.yefeng.base;

import lombok.Getter;


@Getter
public enum BaseCodeEnum {


    /**
     * 系统错误码
     */
    SUCCESS(200, "操作成功"),
    FAILED(400, "操作失败"),
    NOT_FOUND(404, "无效请求"),
    SC_UNAUTHORIZED(401, "请登录！"),
    SC_TOKEN_INVALID(402, "token无效或不存在"),
    FORBIDDEN(403, "拒绝访问"),
    SC_METHOD_NOT_ALLOWED(405, "权限不足"),
    SERVER_ERROR(500, "服务器错误"),
    COMMON_ACCOUNT_ERROR(1001, "账号错误"),
    COMMON_TOKEN_PAST(1002, "accessToken失效"),
    COMMON_TOKEN_ERROR(1003, "accessToken不合法"),
    COMMON_PARAM_EMPTY(1004, "必选参数为空"),
    COMMON_PARAM_ERROR(1005, "参数格式错误"),
    IS_NULL(1006, "信息为空"),
    SERVICE_NOT_EXIST(1007, "服务不存在"),
    SECRET_FAILED(1008, "密钥验证失败"),
    COMMON_PARAM_LOSE(1009, "参数失效"),
    NOT_PHONE(1010, "电话为空"),
    NOT_PAY_CHARGE(1011, "没有购买该服务"),
    NOT_CARD_NUMBER(1012, "身份证为空"),
    ORDER_NOT_EXIST(1013, "订单交易不存在"),
    ORDER_IS_CLOSE(1014, "订单已关闭"),
    ORDER_ERR(1014, "订单交易失败"),
    ORDER_CLOSE_REPEAT(1015, "订单重复结算");

    private final Integer code;
    private final String msg;

    BaseCodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
