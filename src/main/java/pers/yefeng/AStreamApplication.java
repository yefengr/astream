package pers.yefeng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling

public class AStreamApplication {

    public static void main(String[] args) {
        SpringApplication.run(AStreamApplication.class, args);
    }

}
